<?php
$url =  "//".$_SERVER['HTTP_HOST'].dirname(dirname(dirname(dirname(dirname(dirname($_SERVER['REQUEST_URI']))))));
?>




var urlobj;
var modalobj;
function Browse(element, modal){
    urlobj = element;
    modalobj = modal;
    var elfinderUrl = '<?php echo rtrim($url,'/') ?>/explorer/elfinder/standalonepopup/';

    // trigger the reveal modal with elfinder inside
    var triggerUrl = elfinderUrl + 'wtf';
    $.colorbox({
        href: triggerUrl,
        fastIframe: true,
        iframe: true,
        width: '80%',
        height: '80%',
        opacity:0.7
    });
}
// function to update the file selected by elfinder
function processSelectedFile(filePath, requestingField) {
    if(modalobj) {
        var input = modalobj.find('.fileinput')[0];
        $(input).val(filePath).trigger('input');
    }
}