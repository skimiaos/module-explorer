{{-- Explorer activity template
  Implements blocks:
    - page.activity (backend.layouts.default)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.default')

{{-- Page content --}}
@block('page.activity')
    <iframe style="  width: 100%;height: 100%;border: none;" flex os-flex="1"
            src="{{URL::action('Barryvdh\Elfinder\ElfinderController@showIndex')}}"></iframe>
@endoverride