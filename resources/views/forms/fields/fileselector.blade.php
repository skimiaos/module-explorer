<style>.fileinput{display: none;}.materialboxed.active {  box-shadow: 0 0 0 1000em rgba(0, 0, 0, 0.5);  }#materialbox-overlay {background-color: rgba(0,0,0,0)!important}</style>
<?php $idd = uniqid(); ?>

    <script type="text/javascript" src="{{Assets::get('colorbox/jquery.colorbox.js','skimia.explorer')}}"></script>
    <link href="{{Assets::get('colorbox/example5/colorbox.css','skimia.explorer')}}" rel="stylesheet">
<div class="col s12" @if(isset($field['ng-show']))ng-show="{{$field['ng-show']}}"@endif>
<label>{{isset($field['label'])?$field['label']:'Séléction de fichier :'}}</label>
<os-container os-flex direction="row">

        <a os-flex style="margin: 20px" class="btn-floating center btn-large popup_selector" ng-class="{red:form['{{$name}}'] == '', green:form['{{$name}}'] != ''}" data-inputid="feature_image_{{$idd}}"><i class="os-icon-picture"></i></a>
        <input class="fileinput" type="text" id="feature_image_{{$idd}}" name="feature_image"
               @block('form.row.widget.ng-model')ng-model="form['{{$name}}']"@endshow
                />

    <img os-flex class="materialboxed" height="100px"  ng-show="form['<?php echo $name?>']" ng-src="{{form['<?php echo $name?>']}}">
</os-container>
</div>

<script>
    $('.materialboxed').materialbox();
    $(document).on('click','.popup_selector',function (event) {
        event.preventDefault();
        var updateID = $(this).attr('data-inputid'); // Btn id clicked
        var elfinderUrl = '{{url('explorer/elfinder/standalonepopup/')}}/';

        // trigger the reveal modal with elfinder inside
        var triggerUrl = elfinderUrl + updateID;
        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '80%',
            height: '80%',
            opacity:0.7
        });

    });
    // function to update the file selected by elfinder
    function processSelectedFile(filePath, requestingField) {
        $('#' + requestingField).val(filePath).trigger('input');
    }

</script>