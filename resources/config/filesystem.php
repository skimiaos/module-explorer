<?php

return [
    'uploads'=>[
        'dir'=> app_path('uploads'),
        'users'=> true,
        'themes'=> true
    ]
];