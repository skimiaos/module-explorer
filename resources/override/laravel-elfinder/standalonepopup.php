<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>elFinder 2.0</title>

    <!-- jQuery and jQuery UI (REQUIRED) -->
    <link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

    <!-- elFinder CSS (REQUIRED) -->
    <link rel="stylesheet" type="text/css" href="<?= asset($dir . '/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset($dir . '/css/theme.css') ?>">

    <!-- elFinder JS (REQUIRED) -->
    <script src="<?= asset($dir . '/js/elfinder.min.js') ?>"></script>

    <?php if ($locale)
    { ?>
        <!-- elFinder translation (OPTIONAL) -->
        <script src="<?= asset($dir . "/js/i18n/elfinder.$locale.js") ?>"></script>
    <?php } ?>
    <!-- Include jQuery, jQuery UI, elFinder (REQUIRED) -->

    <style>
        .phpdebugbar{
            display: none;
        }
        .elfinder,.elfinder-toolbar,.elfinder-buttonset,.elfinder .elfinder-button,.elfinder-statusbar,.elfinder-navbar .ui-state-active{
            border-radius: 0px!important;
        }
        .ui-widget-header{
            background: none!important;
            background-color: rgb(229, 227, 230)!important;
        }
        .elfinder-navbar{
            background-color: rgb(245, 245, 245)!important;
            padding: 0!important;
        }
        .ui-state-hover{
            background: none!important;
            background-color: rgb(214, 214, 214)!important;
            border-radius: 0px!important;

        }
        .elfinder-navbar .ui-state-hover{
            border:1px solid rgb(214, 214, 214)!important;
        }
    </style>
    <script type="text/javascript">
        $().ready(function () {
            var elf = $('#elfinder').elfinder({
                // set your elFinder options here
                <?php if($locale){ ?>
                    lang: '<?= $locale ?>', // locale
                <?php } ?>
                customData: { 
                    _token: '<?= csrf_token() ?>',
                    restrict_public:true
                },
                url: '<?= URL::action('Barryvdh\Elfinder\ElfinderController@showConnector') ?>',  // connector URL
                dialog: {width: 900, modal: true, title: 'Select a file'},
                resizable: false,
                commandsOptions: {
                    getfile: {
                        oncomplete: 'destroy'
                    }
                },
                height: $( document ).height()-2,
                getFileCallback: function (file) {
                    console.log(file);
                    var path = file.path.split("\\").join("/");
                    path = path.split("\/").join("/");

                    var path_split = path.split('/');
                    path_split.splice(0,1);


                    path = path_split.join("/");


                    window.parent.processSelectedFile(file.baseUrl+path, '<?= $input_id?>');
                    parent.jQuery.colorbox.close();
                }
            }).elfinder('instance');
        });
    </script>


</head>
<body style="margin: 0;overflow: hidden">
<!-- Element where elFinder will be created (REQUIRED) -->
<div id="elfinder"></div>

</body>
</html>
