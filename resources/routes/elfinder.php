<?php
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

Route::group(array('before' => 'auth'), function()
{
    \Route::get('explorer/elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
    \Route::any('explorer/elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
    \Route::get('explorer/elfinder/standalonepopup/{input_id}', 'Barryvdh\Elfinder\ElfinderController@showPopup');
    \Route::get('glide/{path}', function($path){
        $server = \League\Glide\ServerFactory::create([
            'source' => public_path(),
            'cache' => storage_path('glide'),
        ]);
        return $server->getImageResponse($path, Input::query());
    })->where('path', '.+');







});

\View::addNamespace('laravel-elfinder',module_resources('skimia.explorer','override/laravel-elfinder'));

Route::matched(function(){
    $acl = $GLOBALS['app']->make('acl');
    if($acl->can('explore')) {

        $roots = [];

        if(Config::get('skimia.explorer::filesystem.uploads.users')){
            /*******
             *
             * DOSSIERS UTILISATEURS
             *
             */
            $users_homes = Config::get('skimia.explorer::filesystem.uploads.dir');
            if(!File::exists($users_homes))
                File::makeDirectory($users_homes,0777,true);

            $folder = strtolower(strtr(utf8_decode($GLOBALS['app']['auth']->user()->email),
                utf8_decode('���������������������������������������������������'),
                'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
            ));

            if(!File::exists($users_homes.'/'.$folder)){
                File::makeDirectory($users_homes.'/'.$folder,0777,true);
                File::put($users_homes.'/'.$folder.'/bonjour.txt','Vous pouvez des a pr�sent utiliser votre espace personnel.');
            }

            //DROITS

            $roots[] = [
                'driver' => 'Flysystem',
                //'path' => 'uploads',
                'filesystem' => new Filesystem(new Local($users_homes.'/'.$folder)),
                'alias' => 'Dossier Personnel'
            ];

            if($acl->can('explore.root')){
                $roots[] = [
                    'driver' => 'Flysystem',
                    //'path' => 'uploads',
                    'filesystem' => new Filesystem(new Local($users_homes)),
                    'alias' => 'Dossier Utilisateurs'
                ];
            }

        }


        if($acl->can('explore.root')){
            $roots[] = [
                'driver' => 'Flysystem',
                //'path' => 'uploads',
                'filesystem' => new Filesystem(new Local(base_path() . '/themes')),
                'alias' => 'Themes'
            ];
        }

        if($acl->can('explore.public')){
            if(Input::get('restrict_public',false)){
                $roots = [];
            }
            $roots[] = [
                'driver' => 'Flysystem',
                //'path' => 'uploads',
                'URL' => url('/uploads'),
                'filesystem' => new Filesystem(new Local(public_path() . '/uploads')),
                'glideURL' => url('/glide/uploads'),
                'alias' => 'Dossier Publique'
            ];
        }


        Config::set('laravel-elfinder::dir', ['uploads']);
        Config::set('laravel-elfinder::roots', $roots);
    }
});