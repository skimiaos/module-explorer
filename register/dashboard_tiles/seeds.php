<?php
Event::listen('skimia.backend::seed.dashboard.default.sections', function($admin){
    if($admin)
        return ['explorer'=>'Stockage',];
    else
        return ['website'=>'Gestion du Site',];
},1000);
Event::listen('skimia.backend::seed.dashboard.default.tiles', function($admin){

    return [($admin?'explorer':'website')=>[
        'explorer_index'=>[
            'static_id'=> 'explorer',
            'size'=>'large'
        ]
    ]];
});