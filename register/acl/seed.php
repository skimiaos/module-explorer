<?php
Event::listen('artisan.os.install.acl',function($manager){
    $manager->attachActions(['explore']);
    $manager->attachActions(['explore.public']);
    $manager->attachActions(['explore.root']);
    $manager->getAcl()->allow('Member', ['explore','explore.public']);

},9999);