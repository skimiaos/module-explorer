<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\Explorer',
    'require'     => ['skimia.modules','skimia.angular','skimia.auth','skimia.backend']
];