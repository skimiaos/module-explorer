<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/05/2015
 * Time: 15:18
 */

namespace Skimia\Explorer\Controllers;


use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\File;

class FileConnector extends \Controller{


    public function fileManager(){
        require_once module_libs('skimia.explorer','fm/filemanager.php');
        die();
    }

}