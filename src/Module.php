<?php

namespace Skimia\Explorer;


use Skimia\Blade\Facades\BladeHelper;
use Skimia\Modules\ModuleBase;
use BackendNavigation as Nav;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Theme;
use App;


class Module extends ModuleBase{


    public function getAliases(){
        return [

        ];
    }

    /**
     * Lanc� avant le register
     *  au chargement du module
     * faire ici des modifications sur du core
     * * placer ici du code
     */
    public function beforeRegisterModule()
    {



    }
}